"""Test gitlab-yaml-shellcheck."""
import contextlib
import io
import pathlib
import subprocess
import tempfile
from typing import Iterable
from typing import List
from typing import Optional
from typing import Tuple
import unittest
from unittest import mock

from cki_lib import gitlab_yaml_shellcheck


class TestShellCheck(unittest.TestCase):
    """Test cki_lib/gitlab_yaml_shellcheck.py."""

    @staticmethod
    def _run(
        data: str, args: Optional[Iterable[str]] = None, cache: bool = False
    ) -> Tuple[int, List[List[str]], str]:
        list_args = list(args or [])
        if not cache:
            list_args.append('--no-cache')
        with tempfile.NamedTemporaryFile('w') as file:
            file.write(data)
            file.seek(0)
            redirected = io.StringIO()
            with contextlib.redirect_stdout(redirected):
                code = gitlab_yaml_shellcheck.main(list_args + [file.name])
            output = redirected.getvalue().rstrip()
            return code, [line.split(':', 3) for line in
                          output.split('\n')], output

    def _assert_messages(
        self, code: int, messages: List[List[str]], expected: List[Tuple[int, str]]
    ) -> None:
        if not expected:
            self.assertEqual(code, 0)
            return
        for expected_line, expected_sc in expected:
            matches = [m for m in messages
                       if m[1:2] == [str(expected_line)]]
            self.assertEqual(len(matches), 1)
            self.assertIn(expected_sc, matches[0][3])

    def test_line_numbers(self) -> None:
        """Test the line number mapping."""

        code, messages, _ = self._run(
            'job:\n'
            '  script:\n'
            '    - unused=1\n'
        )
        self._assert_messages(code, messages, [(3, 'SC2034')])

    def test_multiple_jobs(self) -> None:
        """Test the merging of multiple jobs."""

        code, messages, _ = self._run(
            'job1:\n'
            '  script:\n'
            '    - unused1=1\n'
            'job2:\n'
            '  script:\n'
            '    - unused2=1\n'
        )
        self._assert_messages(code, messages, [
            (3, 'SC2034'),
            (6, 'SC2034'),
        ])

    def test_global_before_script(self) -> None:
        """Test the merging of global scripts."""

        code, messages, _ = self._run(
            'before_script:\n'
            '  - unused1=1\n'
            'job:\n'
            '  script:\n'
            '    - unused=1\n'
        )
        self._assert_messages(code, messages, [
            (2, 'SC2034'),
            (5, 'SC2034'),
        ])

    def test_extends(self) -> None:
        """Test the merging of extended jobs."""

        code, messages, _ = self._run(
            '.job:\n'
            '  before_script:\n'
            '    - unused1=1\n'
            'job:\n'
            '  extends: .job\n'
            '  script:\n'
            '    - unused2=1\n',
            args=['--job', 'job'],
        )
        self._assert_messages(code, messages, [
            (3, 'SC2034'),
            (7, 'SC2034'),
        ])

    def test_variables(self) -> None:
        """Test the definition of variables."""

        _, _, output = self._run(
            'variables:\n'
            '  var3: baz\n'
            'job:\n'
            '  variables:\n'
            '    var1: baz\n'
            '  script:\n'
            '    - var2=$var1\n'
            '    - echo "$var2"\n'
            '    - echo "$var3"\n'
        )
        self.assertNotIn('SC2154', output)

    def test_predefined_variables(self) -> None:
        """Test the predefined variables."""

        _, _, output = self._run(
            'job:\n'
            '  script:\n'
            '    - var2=$var1\n'
            '    - echo "$var2"\n'
            '    - echo "$var3"\n',
            args=['--predefined', 'var1',
                  '--predefined', 'var3']
        )
        self.assertNotIn('SC2154', output)

    def test_var_redefinition(self) -> None:
        """Test the redefinition of variables."""

        _, _, output = self._run(
            'variables:\n'
            '  var: baz\n'
            'job:\n'
            '  script:\n'
            '    - var=bar\n'
            '    - echo "$var"\n'
            '  after_script:\n'
            '    - echo "$var"\n'
        )
        self.assertNotIn('SC2030', output)
        self.assertNotIn('SC2031', output)

    def test_set_e(self) -> None:
        """Test that set -e is set."""

        _, _, output = self._run(
            'variables:\n'
            '  var: baz\n'
            'job:\n'
            '  script:\n'
            '    - cd /tmp\n'
        )
        self.assertNotIn('SC2164', output)

    def test_cache_invalidate_line_numbers(self) -> None:
        """Test the cache invalidation with changing line numbers."""
        script = ('job:\n'
                  '  script:\n'
                  '    - unused=1\n')
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', directory):
            code, messages, _ = self._run(script, cache=True)
            self._assert_messages(code, messages, [(3, 'SC2034')])
            code, messages, _ = self._run('\n' + script, cache=True)
            self._assert_messages(code, messages, [(4, 'SC2034')])
        self.assertEqual(len(run.mock_calls), 2)

    def test_cache_invalidate_options(self) -> None:
        """Test the cache invalidation with shellcheck command line options."""
        script = ('job:\n'
                  '  script:\n'
                  '    - unused=1\n')
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', directory):
            code, messages, _ = self._run(script, cache=True,
                                          args=('--exclude=SC2154',))
            self._assert_messages(code, messages, [(3, 'SC2034')])
            code, messages, _ = self._run(script, cache=True)
            self._assert_messages(code, messages, [(3, 'SC2034')])
        self.assertEqual(len(run.mock_calls), 2)

    def test_cache_invalidate_check_sourced(self) -> None:
        """Test the cache invalidation when reporting on external scripts."""
        script = ('job:\n'
                  '  script:\n'
                  '    - unused=1\n')
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', directory):
            code, messages, _ = self._run(script, cache=True,
                                          args=('--check-sourced',))
            self._assert_messages(code, messages, [(3, 'SC2034')])
            code, messages, _ = self._run(script, cache=True,
                                          args=('--check-sourced',))
            self._assert_messages(code, messages, [(3, 'SC2034')])
        self.assertEqual(len(run.mock_calls), 2)

    def test_cache(self) -> None:
        """Test the caching of results."""
        script = ('job:\n'
                  '  script:\n'
                  '    - unused=1\n'
                  'job2:\n'
                  '  extends: job\n')
        with mock.patch('subprocess.run', wraps=subprocess.run) as run,\
                tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', directory):
            code, messages, _ = self._run(script, cache=True)
            self._assert_messages(code, messages, [(3, 'SC2034')])
            code, messages, _ = self._run(script, cache=True)
            self._assert_messages(code, messages, [(3, 'SC2034')])
        self.assertEqual(len(run.mock_calls), 1)

    def test_cache_depth(self) -> None:
        """Test the cache cleaning."""
        script = ('job:\n'
                  '  script:\n'
                  '    - unused=1\n')
        with tempfile.TemporaryDirectory() as directory,\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_path', directory),\
                mock.patch('cki_lib.gitlab_yaml_shellcheck'
                           '.YamlShellLinter.cache_depth', 5):
            for count in range(10):
                self._run(('\n' * count) + script, cache=True)
            self.assertEqual(len(list(pathlib.Path(directory).iterdir())), 5)

    def test_check_sourced(self) -> None:
        """Test the --check-sourced parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                'job:\n'
                '  script:\n'
                '    - unused=1\n',
                args=['--check-sourced']
            )
        self.assertIn('-ax', run.mock_calls[0].args[0])

    def test_check_sourced_missing(self) -> None:
        """Test a missing --check-sourced parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                'job:\n'
                '  script:\n'
                '    - unused=1\n'
            )
        self.assertIn('-x', run.mock_calls[0].args[0])

    def test_include(self) -> None:
        """Test the --include parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                'job:\n'
                '  script:\n'
                '    - unused=1\n',
                args=['--include', 'SC2034']
            )
        self.assertIn('--include=SC2034', run.mock_calls[0].args[0])

    def test_exclude(self) -> None:
        """Test the --exclude parameter."""
        with mock.patch('subprocess.run', wraps=subprocess.run) as run:
            self._run(
                'job:\n'
                '  script:\n'
                '    - unused=1\n',
                args=['--exclude', 'SC2034']
            )
        self.assertIn('--exclude=SC2034', run.mock_calls[0].args[0])

    def test_block_style(self) -> None:
        """Test the line number mapping for block style."""

        code, messages, _ = self._run(
            'job:\n'
            '  script:\n'
            '    - |\n'
            '      echo hi\n'
            '      unused=1\n'
        )
        self._assert_messages(code, messages, [(5, 'SC2034')])

    def test_recursive_lists(self) -> None:
        """Test recursive lists."""

        code, messages, _ = self._run(
            '.script: &script\n'
            '  - echo hi\n'
            '  - unused=1\n'
            '\n'
            'job:\n'
            '  script:\n'
            '    - *script\n'
            '    - |\n'
            '      echo hi\n'
        )
        self._assert_messages(code, messages, [(3, 'SC2034')])
