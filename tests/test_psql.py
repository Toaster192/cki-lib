"""Test cki_lib.psql module."""
import unittest
from unittest import mock

from cki_lib import psql


class TestPSQLHandler(unittest.TestCase):
    """Test PSQLHandler."""

    def test_init(self):
        """Test init parameters."""
        cases = [
            (['psql.host', '1234', 'db'], {},
             'host=psql.host port=1234 dbname=db'),
            (['psql.host', '1234', 'db'], {'user': 'usr', 'password': 'pwd', 'require_ssl': True},
             'host=psql.host port=1234 dbname=db user=usr password=pwd sslmode=require'),
        ]

        for args, kwargs, expected in cases:
            handler = psql.PSQLHandler(*args, **kwargs)
            self.assertEqual(expected, handler.connection_string)
            self.assertIsNone(handler._connection)  # pylint: disable=protected-access
            self.assertTrue(handler.autocommit)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection_already_exists(self, mock_connect):
        """Test connection property. _connection already set."""
        handler = psql.PSQLHandler('psql.host', '1234', 'db')
        handler._connection = mock.Mock()  # pylint: disable=protected-access
        handler.connection  # pylint: disable=pointless-statement
        self.assertFalse(mock_connect.called)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection(self, mock_connect):
        """Test connection property."""
        connection = mock.Mock()
        mock_connect.return_value = connection

        handler = psql.PSQLHandler('psql.host', '1234', 'db')
        handler.connection  # pylint: disable=pointless-statement
        self.assertTrue(mock_connect.called)
        connection.set_isolation_level.assert_called_with(0)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection_no_autocommit(self, mock_connect):
        """Test connection property."""
        connection = mock.Mock()
        mock_connect.return_value = connection

        handler = psql.PSQLHandler('psql.host', '1234', 'db', autocommit=False)
        handler.connection  # pylint: disable=pointless-statement

        self.assertTrue(mock_connect.called)
        self.assertFalse(connection.set_isolation_level.called)

    @staticmethod
    @mock.patch('cki_lib.psql.psycopg2.connect', mock.Mock())
    def test_execute():
        """Test execute method."""
        handler = psql.PSQLHandler('host', 1234, 'db')
        handler._connection = mock.MagicMock()

        handler.execute('SELECT %s', 'foo')
        handler.connection.assert_has_calls([
            mock.call.cursor(),
            mock.call.cursor().__enter__(),
            mock.call.cursor().__enter__().execute('SELECT %s', 'foo'),
            mock.call.cursor().__enter__().fetchall(),
            mock.call.cursor().__exit__(None, None, None)
        ])
