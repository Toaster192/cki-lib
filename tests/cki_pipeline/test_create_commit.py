"""Unit tests for cki_pipeline.create_commit()."""
# pylint: disable=protected-access
import unittest

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestCreateCommit(unittest.TestCase, mocks.GitLabMocks):
    """Test cases for utils.create_commit()."""

    @responses.activate
    def test_created_data(self):
        """Verify the content of created commit looks as expected."""

        commit_text = 'title\n\ncki_pipeline_branch = branch\ntitle = title'
        expected_data = {'branch': 'branch',
                         'actions': [],
                         'commit_message': commit_text}

        gitlab = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, 'branch')

        project = gitlab.projects.get('cki-project')
        cki_pipeline._create_commit(project,
                                    {'cki_pipeline_branch': 'branch',
                                     'title': 'title'})

        commit_request = self.get_requests(
            url='https://gitlab.com/api/v4/projects/1/repository/commits')[0]
        self.assertEqual(commit_request, expected_data)
