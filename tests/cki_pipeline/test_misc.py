"""Tests for various methods of cki_pipeline."""
# pylint: disable=protected-access
import unittest

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestMisc(unittest.TestCase, mocks.GitLabMocks):
    """Test cases for various functions in the cki_pipeline module."""

    def _setup_last_pipeline(self):
        self.add_project(1, 'cki-project')
        self.add_branch(1, 'branch')
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&scope=finished',
            json=[{'id': 103}, {'id': 102}, {'id': 101}, {'id': 100}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&scope=finished&status=success',
            json=[{'id': 103}, {'id': 102}, {'id': 100}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/100/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'baseline'}])
        responses.add(  # not successful above
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/101/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'baseline'}])
        responses.add(  # wrong cki_pipeline_type
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/102/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'other'}])
        responses.add(  # retriggered
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/103/variables',
            json=[{'key': 'cki_pipeline_type', 'value': 'baseline'},
                  {'key': 'retrigger', 'value': 'true'}])

        return get_instance('https://gitlab.com').projects.get('cki-project')

    @responses.activate
    def test_last_pipeline(self):
        """Check the last pipeline without filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_pipeline_for_branch(
            project, 'branch',
            list_filter={"scope": 'finished'}
        ).id, 102)

    @responses.activate
    def test_last_pipeline_filter(self):
        """Check the last pipeline with filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': 'baseline'},
            list_filter={"scope": 'finished'}
        ).id, 101)

    @responses.activate
    def test_last_successful_pipeline(self):
        """Check the last successful pipeline without filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
        ).id, 102)

    @responses.activate
    def test_last_successful_pipeline_filter(self):
        """Check the last successful pipeline with filter."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': 'baseline'},
        ).id, 100)

    @responses.activate
    def test_last_successful_pipeline_missing(self):
        """Check a missing last successful pipeline."""
        project = self._setup_last_pipeline()
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, 'branch',
            variable_filter={'cki_pipeline_type': 'non-existing'},
        ), None)

    def test_clean_project_url(self):
        """Verify the conversion of repository URLs into GitLab project URLs."""

        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test.git/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test.git"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("foo.bar/test.git/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("git+https://foo.bar/test.git/"),
                         "https://foo.bar/test")
