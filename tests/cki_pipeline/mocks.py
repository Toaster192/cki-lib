"""Mock GitLab API responses."""
import base64
import json

import responses


class GitLabMocks:
    """Mock GitLab API responses."""

    @staticmethod
    def add_project(project_id, project_path):
        """Add a mock for a project."""
        for url_id in (project_path.replace('/', '%2F'), project_id):
            responses.add(
                responses.GET,
                f'https://gitlab.com/api/v4/projects/{url_id}',
                json={'id': project_id,
                      'web_url': f'https://gitlab.com/{project_path}',
                      'path_with_namespace': project_path})
        responses.add(
            responses.POST,
            f'https://gitlab.com/api/v4/projects/{project_id}/repository/commits',
            json={})
        responses.add(
            responses.POST,
            f'https://gitlab.com/api/v4/projects/{project_id}/trigger/pipeline',
            json={'web_url': 'pipeline-url'})
        responses.add(
            responses.POST,
            f'https://gitlab.com/api/v4/projects/{project_id}/pipeline',
            json={'web_url': 'pipeline-url'})

    @staticmethod
    def add_branch(project_id, branch_name):
        """Add a mock for a project branch."""
        responses.add(
            responses.GET,
            f'https://gitlab.com/api/v4/projects/{project_id}/repository/branches/{branch_name}',
            json={})
        responses.add(
            responses.DELETE,
            f'https://gitlab.com/api/v4/projects/{project_id}/repository/branches/{branch_name}',
            json={})

    @staticmethod
    def add_gitlab_yml(project_id):
        """Add a mock for .gitlab-ci.yml file."""
        responses.add(
            responses.GET,
            f'https://gitlab.com/api/v4/projects/{project_id}/repository/files/'
            f'%2Egitlab-ci%2Eyml?ref=sha',
            json={'content': base64.b64encode(b'content').decode('utf8')})

    @staticmethod
    def add_pipeline(project_id, pipeline_id, variables):
        """Add a mock for a pipeline."""
        responses.add(
            responses.GET,
            f'https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}',
            json={'id': pipeline_id,
                  'web_url': 'pipeline-url',
                  'sha': 'sha'})
        responses.add(
            responses.GET,
            f'https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}/variables',
            json=[{'key': k, 'value': v} for k, v in variables.items()])

    @staticmethod
    def get_requests(method=None, url=None, parse=True):
        """Return the matching requests."""
        return [json.loads(c.request.body) if parse else c.request.body
                for c in responses.calls
                if (not method or c.request.method == method) and
                (not url or c.request.url == url)]
