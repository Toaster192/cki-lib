"""Tests for FailureRegexes class, that wraps regexes that indicate issues."""
import importlib
import unittest


class TestFailureRegexes(unittest.TestCase):
    """Test Gitlab class."""

    def setUp(self):
        """SetUp class."""
        mod = 'cki_lib.failure_regexes'
        self.failure_regexes = importlib.import_module(mod)
        self.fail_regex_class = self.failure_regexes.FailureRegexes

    def test_init(self):
        """Test that regexes exist as a dict."""
        regexes_class = self.failure_regexes.FailureRegexes
        self.assertIsInstance(regexes_class.regexes, dict)

    def test_match(self):
        """Ensure error string is matched and a description is returned."""
        err_string = 'connection reset by peer'
        hints, description = self.fail_regex_class.match_signature(err_string)

        self.assertIsNone(hints)
        self.assertEqual(description, 'A networking issue')

    def test_no_match(self):
        """Ensure unknown error string produces no results."""
        hints, description = self.fail_regex_class.match_signature('nothing')
        self.assertIsNone(hints)
        self.assertIsNone(description)
