"""Pipeline message helper."""
import mailbox
import re

from dateutil.parser import parse as date_parse

from cki_lib.logger import file_logger

CKI_PROJECT = 'cki-project/cki-pipeline'
BREW_PROJECT = 'cki-project/brew-pipeline'

# put very bad problems to errors.log
ERR_LOGGER = file_logger('errors', dst_file='errors.log')
# put most info messages to info.log
LOGGER = file_logger(__name__)
# put info about messages we couldn't process to skipped.log
SKIPPED_LOGGER = file_logger('skipped', dst_file='skipped.log')


class PipelineMsgBase:
    # pylint: disable=too-many-instance-attributes
    """Parse pipeline message."""

    rgx_pipeline_project = \
        re.compile(r'.*https://(.*?)/(.*?)/pipelines/([0-9]+).*')

    def __init__(self, msg):
        # pylint: disable=W0702
        r"""Construct the object.

        Sets attribute to None if we can't get it from message and logs this.

        Arguments:
            msg: one of mbox messages from mailbox.mbox()
        Attributes:
            msg: one of mbox messages from mailbox.mbox()
            pipelineid: int or None, pipelineid of the pipeline that ran
            revision_iid: int or None, revision_iid of the reported revision
            msgid: str, Message-ID/Message-Id from headers
            subject: str or None, message subject with \t\n replaced with ' '
            inreplyto: str, In-Reply-To from headers
            body: str, email body, multiparts joined
            tdelivered: datetime, parsed 'Date' from headers

        """
        # save mailbox
        self.msg = msg

        try:
            # get pipelineid from header or None
            self.pipelineid = int(msg['X-Gitlab-Pipeline-ID'])
        except (AttributeError, TypeError, ValueError):
            self.pipelineid = None

        try:
            self.revision_iid = int(msg['X-DataWarehouse-Revision-IID'])
        except (AttributeError, TypeError, ValueError):
            self.revision_iid = None

        self.msgid = msg['Message-ID'] if msg['Message-ID'] else \
            msg['Message-Id']

        if msg['Subject']:
            self.subject = msg['Subject'].replace('\t', ' ').\
                replace('\n', ' ').replace('  ', ' ')
        else:
            LOGGER.info('Cannot get subject of %s', self.msgid)
            self.subject = None

        self.inreplyto = msg['In-Reply-To']

        # parse message body, including multipart
        self.body = self._get_msg_body(msg)

        try:
            # Sat, 01 Dec 2018 04:54:46 +0530 (CEST|EST|EDT)
            self.tdelivered = date_parse(msg['Date'])
        except:  # noqa: E722
            self.tdelivered = None
            LOGGER.info('Cannot parse "%s" for %s', msg['Date'],
                        self.subject)

    def get_instance_project(self, xci_url):  # noqa: C901
        # pylint: disable=R1705
        """Get (server instance, project) for a report message with headers.

        Parses the current message (self.msg). Because email headers were
        changed during life-time of the project, the parsing is unnecessarily
        complicated. If we're OK with not being to parse some old data anytime,
        we could simplify this a lot.

        Returns:
            (server instance, project) or (None, None)

        """
        # Option 1: X-Gitlab-Pipeline that was removed...
        x_gitlab_pipeline = self.msg['X-Gitlab-Pipeline']
        if x_gitlab_pipeline:
            try:
                # get field from headers
                res = self.rgx_pipeline_project.fullmatch(x_gitlab_pipeline)
                instance, project, _ = res.groups()
                return instance, project
            except (AttributeError, TypeError, ValueError):
                pass

        # Option 2: pipeline link that is available later on, except in
        # restricted templates...
        for line in self.body.splitlines():
            res = self.rgx_pipeline_project.fullmatch(line)
            if res:
                instance, project, _ = res.groups()
                return instance, project

        # Option 3: headers that were added after Option 1 -> Option 2 change
        url = self.msg['X-Gitlab-Url']
        path = self.msg['X-Gitlab-Path']
        if url and path:
            try:
                if path.count('/') < 4:
                    # misformatted path
                    raise AttributeError

                instance = url.split('//')[1].split('/')[0]
                project = path.split('/')[1] + '/' + path.split('/')[2]
                return instance, project
            except (AttributeError, TypeError, ValueError):
                pass

        # Option 4: fallback, getting desperate
        if 'task: ' in self.body or 'COPR' in self.body or \
                'Koji:' in self.body:
            return xci_url, BREW_PROJECT
        elif 'recent commit from this kernel tree' in self.body or \
                'Commit: ' in self.body:
            return xci_url, CKI_PROJECT
        else:
            ERR_LOGGER.warning('Cannot get instance/project of "%s"',
                               self.subject)
            return None, None

    @classmethod
    def from_file(cls, fname):
        # pylint: disable=W0702
        """Craft cls from objects in mailbox.mbox at fname path.

        Catches all exception from cls constructor and logs only that message
        couldn't be parsed.

        Attributes:
            path: str, path to mbox file
        Returns:
            list of cls instances

        """
        results = []
        for msg in mailbox.mbox(fname):
            try:
                results.append(cls(msg))
            except:  # noqa: E722
                ERR_LOGGER.info("Failed to parse message %s", msg['Subject'])

        return results

    def _get_msg_body(self, msg):
        """Get body of email, convert multiple parts to one string.

        The attachements are ignored. The method tries to handle different
        charsets, but 'utf-8' is default.
        Shouldn't be called from outside. It can be frowned upon that this
        method is run in the constructor, but this makes sure that object
        attributes are mostly the same throughout object life-time.

        Returns:
            body: str, body of the email

        """
        # pylint: disable=W0702
        if msg.is_multipart():
            data_parts = []
            for part in msg.get_payload():
                part_payload = part.get_payload(decode=True)

                if isinstance(part_payload, list):
                    data_parts += [self._get_msg_body(m) for m in part_payload]
                elif isinstance(part_payload, bytes):
                    dispos = part.get("Content-Disposition", None)
                    if dispos:
                        # skip parts of email that are attached files
                        if ('attachment' in dispos or 'inline' in dispos)\
                                and ('filename' in dispos):

                            continue

                    try:
                        part_payload = part_payload.decode('utf-8')
                    except:  # noqa: E722
                        charset = part.get_charset()
                        if charset is None:
                            SKIPPED_LOGGER.info(
                                'SKIPPED: data part of %s\n%s', self.subject,
                                part_payload)

                            continue

                    data_parts.append(part_payload)
                else:
                    data_parts.append(part_payload)

            data_parts = list(filter(lambda e: e, data_parts))
            content = ''.join(data_parts)
        else:
            content = msg.get_payload()

        return content
