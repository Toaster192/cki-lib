"""PostgreSQL Connection Handler."""
import psycopg2


class PSQLHandler:
    """PostgreSQL connection handler."""

    def __init__(self,
                 host, port, dbname, *,
                 user=None, password=None, require_ssl=False, autocommit=True):
        # pylint: disable=too-many-arguments
        """
        Initialize PostgreSQL connection.

        Parameters
            - host: PostgreSQL host (required).
            - port: PostgreSQL port (required).
            - dbname: Database name (required).
            - user: Username for authentication. Default: `None`.
            - password: Password for authentication. Default: `None`.
            - require_ssl: Set sslmode=require. Default: `False`.
            - autocommit: Enable isolation level: Autocommit. Default: `True`.
        """
        self.connection_string = f"host={host} port={port} dbname={dbname}"
        if user:
            self.connection_string += f" user={user}"
        if password:
            self.connection_string += f" password={password}"
        if require_ssl:
            self.connection_string += " sslmode=require"

        self.autocommit = autocommit
        self._connection = None

    @property
    def connection(self):
        """
        Get DB connection.

        Split from init for simpler testing.
        """
        if not self._connection:
            self._connection = psycopg2.connect(self.connection_string)

            if self.autocommit:
                self._connection.set_isolation_level(
                    psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
                )

        return self._connection

    def execute(self, query, *args):
        """Execute a given query."""
        with self.connection.cursor() as cursor:
            cursor.execute(query, *args)
            return cursor.fetchall()
