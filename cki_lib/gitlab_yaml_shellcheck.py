"""Run .gitlab-ci.yml code through shellcheck."""
from __future__ import annotations

import argparse
import copy
import functools
import hashlib
import io
import json
import os
import pathlib
import subprocess
import sys
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import TYPE_CHECKING
from typing import Tuple
from typing import Union

import yaml.loader


class LineStr(str):
    """String that can keep track of a line number."""

    def __new__(cls, value: str, line: Optional[int] = None) -> LineStr:
        # pylint: disable=unused-argument
        """Create a new instance of the class."""
        return str.__new__(cls, value)

    def __init__(self, value: str, line: Optional[int] = None) -> None:
        # pylint: disable=unused-argument
        """Initialize and keep track of the line number if provided."""
        super().__init__()
        self.line = line

    def split(self, *args: Any, **kwargs: Any) -> List[str]:
        """Split and keep track of the line number."""
        if TYPE_CHECKING:
            assert self.line is not None
        return [LineStr(l, self.line + i)
                for i, l in enumerate(super().split(*args, **kwargs))]


class LineTrackingLoader(yaml.loader.SafeLoader):
    # pylint: disable=too-many-ancestors
    """YAML loader that keeps track of source lines."""

    def construct_scalar(self, node: yaml.nodes.ScalarNode) -> str:
        """Keep the source line on everything that looks like a string."""
        if isinstance(node.value, str):
            # - 1-based line numbers for inline and block scalars
            # - for block scalars, start_mark refers to the line of the |,
            #   while the value starts only on the next line
            increment = 2 if node.style else 1
            return LineStr(node.value, node.start_mark.line + increment)
        return super().construct_scalar(node)


class YamlShellLinter:
    # pylint: disable=too-many-instance-attributes
    """Lint the shell code in a .gitlab-ci.yml file."""

    cache_path = os.path.expanduser('~/.cache/gitlab-yaml-shellcheck')
    cache_depth = 500

    def __init__(self, file: io.TextIOWrapper, check_sourced: bool, verbose:
                 bool, include: str, exclude: str, predefined: List[str]):
        # pylint: disable=too-many-arguments
        """Read the pipeline yaml."""
        self._yaml_name = file.name
        with file:
            self._top_level = yaml.load(file, Loader=LineTrackingLoader)
        self._check_sourced = check_sourced
        self._verbose = verbose
        self._include = include
        self._exclude = exclude
        self._predefined = predefined

        self._filesystem_cache: Dict[str, Tuple[int, List[str]]] = {}
        self._memory_cache: Dict[str, Tuple[int, List[str]]] = {}

    @staticmethod
    def merge(results: List[Tuple[int, List[str]]]) -> Tuple[int, List[str]]:
        """Sensibly merge multiple tuples of (returncode, messages).

        Returns a tuple of (highest returncode, messages).
        """
        return functools.reduce(
            lambda a, b: (max(a[0], b[0]), list(set(a[1]) | set(b[1]))), results)

    def all_job_names(self) -> List[str]:
        """Return a list of the names of all jobs."""
        return [k for k in self._top_level.keys()
                if isinstance(self._top_level[k], dict)]

    def _merge_extends(self, key: str) -> Dict[str, Any]:
        result = {}
        extends = self._top_level[key].get('extends')
        if extends:
            result.update(self._merge_extends(extends))
        result.update(copy.deepcopy(self._top_level[key]))
        result.pop('extends', None)
        return result

    def lint_job(self, job_name: str) -> Tuple[int, List[str]]:
        """Run the linter on the given jobs of the yaml.

        Returns a tuple of (highest returncode, messages).
        """
        job = self._merge_extends(job_name)
        variables = self._get_dict((job, self._top_level), 'variables')
        before_script = self._clean_script(
            self._get_array((job, self._top_level), 'before_script'))
        script = self._clean_script(
            self._get_array((job, self._top_level), 'script'))
        after_script = self._clean_script(
            self._get_array((job, self._top_level), 'after_script'))

        exports = [f'export {v}=$UNKNOWN'
                   for v in list(variables.keys()) + self._predefined]
        common = (
            ['#!/bin/bash'] +
            ['### setup'] +
            ['set -eo pipefail'] +  # gitlab-runner/shells/bash.go:writeScript
            ['### variables'] + exports
        )

        results = [self._lint_script(f'{job_name}.script', (
            common +
            ['### before_script'] + before_script +
            ['### script'] + script +
            ['### end']
        ))]
        if after_script:
            results += [self._lint_script(f'{job_name}.after_script', (
                common +
                ['### after_script'] + after_script +
                ['### end']
            ))]
        return self.merge(results)

    def load_cache(self) -> None:
        """Load old shellcheck results from the filesystem."""
        for path in pathlib.Path(self.cache_path).glob('*.json'):
            self._filesystem_cache[path.stem] = json.loads(
                path.read_bytes())

    def save_cache(self) -> None:
        """Save shellcheck results to the filesystem."""
        root_path = pathlib.Path(self.cache_path)
        root_path.mkdir(parents=True, exist_ok=True)
        for key, value in self._memory_cache.items():
            root_path.joinpath(f'{key}.json').write_text(json.dumps(value))
        paths = sorted(root_path.iterdir(), key=os.path.getmtime)
        for path in paths[0:-self.cache_depth]:
            path.unlink()

    @staticmethod
    def _cache_key(options: List[str], script: List[str]) -> str:
        """Determine a unique cache key for the script.

        In the cache key, include all aspects that might have influence on the
        shellcheck output:
        - the script contents
        - the line numbers of the script
        - the shellcheck options
        """
        option_lines = [' '.join(options)]
        script_lines = [f'{line}:{getattr(line, "line", -1)}'
                        for line in script]
        concatenated = '\n'.join(option_lines + script_lines).encode('utf-8')
        return hashlib.md5(concatenated).hexdigest()

    def _get_cache(self, options: List[str], script: List[str]
                   ) -> Optional[Tuple[int, List[str]]]:
        digest = self._cache_key(options, script)
        if digest in self._memory_cache:
            return self._memory_cache[digest]
        if digest in self._filesystem_cache:
            return self._filesystem_cache[digest]
        return None

    def _put_cache(self, options: List[str], script: List[str], result:
                   Tuple[int, List[str]]) -> Tuple[int, List[str]]:
        digest = self._cache_key(options, script)
        self._memory_cache[digest] = result
        return result

    def _lint_script(self, job: str, script: List[str]) -> Tuple[int, List[str]]:
        cmd = ['shellcheck', '-f', 'gcc', '-']
        if self._check_sourced:
            cmd += ['-ax']
        else:
            cmd += ['-x']
        options = []
        if self._include:
            options += [f'--include={self._include}']
        if self._exclude:
            options += [f'--exclude={self._exclude}']

        cached = self._get_cache(options, script)
        if cached:
            return cached

        if self._verbose:
            print(f'Linting {job} via {" ".join(cmd + options)}')
        output = subprocess.run(cmd + options,
                                input='\n'.join(script),
                                encoding='utf8',
                                capture_output=True,
                                check=False)
        messages = [self._mangle_shellcheck_output(script, line)
                    for line in output.stdout.split('\n')
                    if line]
        return self._put_cache(options, script, (output.returncode, messages))

    def _mangle_shellcheck_output(self, script: List[str], line: str) -> str:
        parts = line.split(':', 3)
        if len(parts) < 4:
            return line
        name, line, column, message = parts[:4]
        if name == '-':
            column = '1'
            name = self._yaml_name
            if int(line) in range(len(script)):
                line = str(getattr(script[int(line) - 1], 'line', -1))
            else:
                line = '-1'
        return ':'.join([name, line, column, message])

    @staticmethod
    def _get_array(yaml_dicts: Tuple[Dict[str, Any], Dict[str, Any]],
                   field_name: str) -> List[str]:
        """Retrieve an array from a field of different dicts of the yaml."""
        for part in yaml_dicts:
            if field_name in part:
                return part[field_name]
        return []

    @staticmethod
    def _get_dict(yaml_dicts: Tuple[Dict[str, Any], Dict[str, Any]],
                  field_name: str) -> Dict[str, Any]:
        """Assemble a dict from a field of different dicts of the yaml."""
        result = {}
        for part in reversed(yaml_dicts):
            if field_name in part:
                result.update(part[field_name])
        return result

    @staticmethod
    def _clean_script(parts: Union[str, List[str]]) -> List[str]:
        """Flatten a yaml script array."""
        if isinstance(parts, str):
            return parts.split('\n')
        return [line for part in parts for line in YamlShellLinter._clean_script(part)]


def main(argv: Optional[List[str]] = None) -> int:
    """Run the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=argparse.FileType('r'),
                        help='GitLab CI/CD pipeline file')
    parser.add_argument('--job', action='append',
                        help='Only lint the given jobs')
    parser.add_argument('--check-sourced', action='store_true',
                        help='show warnings from followed "source" commands')
    parser.add_argument('--include', metavar='CODE1,CODE2...',
                        help='Consider only given types of warnings')
    parser.add_argument('--exclude', metavar='CODE1,CODE2...',
                        help='Exclude types of warnings')
    parser.add_argument('--predefined', action='append', default=[],
                        help='Declare predefined variables')
    parser.add_argument('--verbose', action='store_true',
                        help='provide progress information')
    parser.add_argument('--no-cache', action='store_true',
                        help='do not cache shellcheck output between calls')
    args = parser.parse_args(argv or sys.argv[1:])

    linter = YamlShellLinter(args.file,
                             check_sourced=args.check_sourced,
                             verbose=args.verbose,
                             include=args.include,
                             exclude=args.exclude,
                             predefined=args.predefined)
    if not args.check_sourced and not args.no_cache:
        linter.load_cache()
    job_names = args.job or linter.all_job_names()
    returncode, messages = linter.merge([linter.lint_job(job_name) for job_name
                                         in job_names])
    if not args.check_sourced and not args.no_cache:
        linter.save_cache()

    print('\n'.join(messages))
    return returncode


if __name__ == '__main__':
    sys.exit(main())
