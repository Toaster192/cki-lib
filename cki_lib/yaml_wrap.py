"""Wrap yaml serializer to work in the way we need it to."""
from abc import ABCMeta
from abc import abstractmethod

from ruamel.yaml import YAML


class Serializer(metaclass=ABCMeta):
    """Required interface for yaml serializer."""

    @abstractmethod
    def deserialize_file(self, fname):
        """Open file fname, return object representing the parsed file."""

    @abstractmethod
    def serialize(self, obj, stream):
        """Serialize obj, write output to stream."""


class RuamelSerializer(Serializer):
    """Wrapper for ruamel yaml (de)serializer."""

    def __init__(self):
        """Create the object."""
        self.yaml = YAML()
        self.yaml.preserve_quotes = True

    def serialize(self, obj, stream):
        """Serialize obj, write output to stream."""
        self.yaml.indent(mapping=2, sequence=4, offset=2)

        self.yaml.dump(obj, stream)

    def deserialize_file(self, fname):
        """Open file fname, return object representing the parsed file."""
        with open(fname, 'rb') as stream:
            parsed_yaml = self.yaml.load(stream.read())
        return parsed_yaml
